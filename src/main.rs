extern crate iridium_lib;

use std::io::{self, Write, stdout};
use std::env::args;
use std::fs::File;
use std::io::prelude::*;

use iridium_lib::repl;
use iridium_lib::iris;
use iridium_lib::iasm;
use iridium_lib::vm;
fn main() {
    let mut evaluator = iris::compiler::Evaluator::new();
    if args().len() == 2 {
        // let mut scheduler = vm::ivm::scheduler::Scheduler::new();
        // TODO: Better error handling here
        let file = File::open(args().nth(1).unwrap());
        match file {
            Ok(mut f) => {
                let mut contents = String::new();
                let result = f.read_to_string(&mut contents);
                match result {
                    Ok(_) => {
                        evaluator.compile(contents);
                        let lexed_results = iasm::lexer::Lexer::lex_tokens(evaluator.assembled()
                                .as_bytes())
                            .to_result()
                            .unwrap();
                        let tokens = iasm::lexer::token::Tokens::new(&lexed_results);
                        let program = iasm::parser::Parser::parse_tokens(tokens).to_result();
                        let mut binary = iasm::assembler::IASM::new();
                        binary.assemble(program.unwrap());
                        let mut new_ivm = vm::ivm::IVM::new();
                        new_ivm.load_binary(binary.clone_binary().unwrap());
                        new_ivm.start();
                        while new_ivm.execute_one() {}
                    }
                    Err(e) => {
                        println!("Error reading file: {:?}", e);
                    }
                }
            }
            Err(e) => {
                println!("Error opening file: {:?}", e);
            }
        }
        return;
    }

    println!("Welcome to Iridium! Let's be productive!");
    let mut repl = repl::Repl::new();
    loop {
        print!(">> ");
        let result = stdout().flush();
        if result.is_err() {
            println!("Error flushing stdout: {:?}", result);
            continue;
        }
        let mut buffer = String::new();
        match io::stdin().read_line(&mut buffer) {
            Ok(_) => {
                buffer.pop();
                repl.process(&buffer);
            }
            Err(error) => println!("error: {}", error),
        }
    }
}
