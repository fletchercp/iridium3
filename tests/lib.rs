extern crate iridium_lib;

use iridium_lib::iasm;
use iridium_lib::iasm::parser::ast;
use iridium_lib::vm::ivm::IVM;

#[test]
fn asm_simple() {
    let mut asm = iasm::assembler::IASM::new();
    let inst = ast::Instruction::OpcodeInst {
        label: Some(ast::Label {
            name: Some(String::from("test")),
            position: Some(0),
        }),
        opcode: ast::Opcode::LOD,
        operands: vec![ast::Operand::Register(0), ast::Operand::IntLiteral(10)],
    };
    let inst2 = ast::Instruction::OpcodeInst {
        label: None,
        opcode: ast::Opcode::JMP,
        operands: vec![ast::Operand::StringLiteral(String::from("test"))],
    };
    let mut p: ast::Program = ast::Program::new();
    p.push(inst);
    p.push(inst2);
    asm.assemble(p);

    let mut vm = IVM::new();
    vm.load_binary(asm.clone_binary().unwrap());
    vm.execute_one();
    vm.execute_one();
}

// #[test]
// fn asm_loop() {
//     let mut asm = iasm::assembler::IASM::new();
//     let inst = ast::Instruction::OpcodeInst {
//         label: None,
//         opcode: ast::Opcode::LOD,
//         operands: vec![ast::Operand::Register(0), ast::Operand::IntLiteral(0)],
//     };
//     let inst6 = ast::Instruction::OpcodeInst {
//         label: None,
//         opcode: ast::Opcode::LOD,
//         operands: vec![ast::Operand::Register(10), ast::Operand::IntLiteral(1)],
//     };
//     let inst2 = ast::Instruction::OpcodeInst {
//         label: None,
//         opcode: ast::Opcode::ADD,
//         operands: vec![ast::Operand::Register(0),
//                        ast::Operand::Register(0),
//                        ast::Operand::Register(10)],
//     };
//     let inst3 = ast::Instruction::OpcodeInst {
//         label: None,
//         opcode: ast::Opcode::SLP,
//         operands: vec![ast::Operand::IntLiteral(5000)],
//     };
//     let inst4 = ast::Instruction::OpcodeInst {
//         label: None,
//         opcode: ast::Opcode::CMP,
//         operands: vec![ast::Operand::Register(0), ast::Operand::Register(10)],
//     };
//     let inst5 = ast::Instruction::OpcodeInst {
//         label: None,
//         opcode: ast::Opcode::JNEQ,
//         operands: vec![ast::Operand::IntLiteral(-4)],
//     };

//     let mut p: ast::Program = ast::Program::new();
//     p.push(inst);
//     p.push(inst6);
//     p.push(inst2);
//     p.push(inst3);
//     p.push(inst4);
//     p.push(inst5);
//     asm.assemble(p);

//     let mut vm = IVM::new();
//     vm.load_binary(asm.clone_binary().unwrap());
//     vm.execute_one();
//     vm.execute_one();
//     vm.execute_one();
//     vm.execute_one();
//     assert_eq!(vm.active, false);
//     vm.execute_one();
//     assert_eq!(vm.zf, 0);
//     vm.execute_one();
//     assert_eq!(vm.pc, 8);
//     vm.execute_one();
//     assert_eq!(vm.registers[0].value, 3);
//     vm.execute_one();
//     vm.execute_one();
//     vm.execute_one();
//     vm.execute_one();
//     assert_eq!(vm.pc, 21);
// }