use vm::ivm::ir_object::{IRObject, IRType};
use std::collections::HashMap;

/// Heap represents the Heap of a Program. Its just a HashMap with i64 as the key
/// and IRObjects as the values
#[derive(PartialEq, Debug, Clone)]
pub struct Heap {
    memory: HashMap<i64, IRObject>,
    pub counter: i64,
}

impl Heap {
    pub fn new() -> Heap {
        Heap {
            memory: HashMap::new(),
            counter: 0,
        }
    }

    /// Allocates a new IRObject and returns the address at which it lives
    pub fn new_obj(&mut self, t: IRType) -> i64 {
        let address = self.counter;
        self.memory.insert(self.counter, IRObject::new(t));
        self.counter += 1;
        address
    }

    /// Gets the IRObject present at an address in the heap
    pub fn get_obj(&mut self, address: i64) -> Option<&mut IRObject> {
        self.memory.get_mut(&address)
    }
}