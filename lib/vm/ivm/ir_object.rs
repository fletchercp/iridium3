use std::collections::HashMap;

/// IRObject is a variant type that is used for heap-allocated objects
#[derive(PartialEq, Debug, Clone)]
pub struct IRObject {
    pub ir_type: IRType,
    string_data: Option<String>,
    number_data: Option<i64>,
    array_data: Vec<IRObject>,
    hash_data: HashMap<i64, IRObject>,
}

#[derive(PartialEq, Debug, Clone)]
pub enum IRType {
    IRString,
    Number,
    Array,
    Hash,
}

impl IRObject {
    pub fn new(t: IRType) -> IRObject {
        IRObject {
            ir_type: t,
            string_data: None,
            number_data: None,
            array_data: Vec::new(),
            hash_data: HashMap::new(),
        }
    }

    pub fn set_string(&mut self, s: &str) {
        self.string_data = Some(s.to_owned());
    }

    pub fn set_number(&mut self, n: i64) {
        self.number_data = Some(n);
    }

    pub fn add_array_object(&mut self, obj: IRObject) {
        self.array_data.push(obj);
    }

    pub fn add_hash_object(&mut self, address: i64, obj: IRObject) {
        self.hash_data.insert(address, obj);
    }
}