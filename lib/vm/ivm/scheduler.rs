use vm::ivm;
use std::{thread, time};
use std::sync::{Arc, Mutex};
use num_cpus;

pub struct Scheduler {
    ivms: Arc<Mutex<Vec<ivm::IVM>>>,
    cpus: usize,
}

impl Scheduler {
    pub fn new() -> Scheduler {
        let new_scheduler = Scheduler{
            ivms: Arc::new(Mutex::new(Vec::new())),
            cpus: num_cpus::get(),
        };
        new_scheduler
    }

    pub fn add_ivm(&mut self, ivm: ivm::IVM) {
        let ivm_vec = self.ivms.clone();
        ivm_vec.lock().unwrap().insert(0, ivm);
    }

    pub fn next_ivm(&mut self) -> Option<ivm::IVM> {
        let ivm_vec = self.ivms.clone();
        let t = ivm_vec.lock().unwrap().pop();
        t
    }

    pub fn cpus(&self) -> usize {
        self.cpus
    }


    pub fn spawn(&mut self) {
        println!("Starting thread!");
        let clone = self.ivms.clone();
        thread::spawn(|| {
            schedule(clone);
        });
    }
}
 
fn schedule(ivm_vec: Arc<Mutex<Vec<ivm::IVM>>>) {
    println!("Beginning schedule function...");
    let sleep_period = time::Duration::from_millis(10);
    
    loop {
        let vm = ivm_vec.lock().unwrap().pop();
        match vm {
            Some(mut ivm) => {
                if !ivm.executable() {
                    thread::sleep(sleep_period);
                    let l = ivm_vec.lock();
                    match l {
                        Ok(mut lock) => {
                            lock.insert(0, ivm);
                        },
                        Err(e) => {
                            println!("Unable to acquire lock: {:?}", e);
                        }
                    }
                    continue;
                }
                if !ivm.active && ivm.wake_time.is_some() {
                    let now = time::Instant::now();
                    if now >= ivm.wake_time.unwrap() {
                        ivm.active = true;
                    } else {
                        let l = ivm_vec.lock();
                        match l {
                            Ok(mut lock) => {
                                lock.insert(0, ivm);
                            },
                            Err(e) => {
                                println!("Unable to acquire lock: {:?}", e);
                            }
                        }
                        continue;
                    }
                }

                for _ in 0..2000 {
                    if !ivm.active && ivm.wake_time.is_some() {
                        let now = time::Instant::now();
                        if now >= ivm.wake_time.unwrap() {
                            ivm.active = true;
                        } else {
                            continue;
                        }
                    }
                    ivm.execute_one();
                    
                }
                let l = ivm_vec.lock();
                match l {
                    Ok(mut lock) => {
                        lock.insert(0, ivm);
                    },
                    Err(e) => {
                        println!("Unable to acquire lock: {:?}", e);
                    }
                }
            },
            None => {
                thread::sleep(sleep_period);
                continue;
            }
        }
    }
}