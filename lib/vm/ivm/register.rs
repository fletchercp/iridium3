#[derive(PartialEq, Debug, Clone)]
pub struct Register {
    pub value: i64,
}

impl Register {
    pub fn new() -> Register {
        Register{
            value: 0,
        }
    }
}
