pub mod register;
pub mod scheduler;
pub mod ir_object;

use byteorder::{BigEndian, ReadBytesExt};
use std::io::Cursor;
use std::{thread, time};

// Iridium plugins
use iasm::assembler::asm::Binary;
use iasm::parser::ast::{Opcode, int_to_opcode};
use vm::ivm::register::*;
use vm::ivm::ir_object::*;
use vm::heap::Heap;

const NUM_REGISTERS: usize = 255;

#[derive(PartialEq, Debug, Clone)]
pub struct IVM {
    binary: Option<Binary>,
    pub pc: u64,
    pub zf: i64,
    cf: i64,
    pub registers: Vec<Register>,
    pub active: bool,
    pub heap: Heap,
    wake_time: Option<time::Instant>,
}

impl IVM {
    pub fn new() -> IVM {
        let mut ivm = IVM {
            binary: None,
            pc: 0,
            zf: -1,
            cf: 0,
            active: false,
            registers: Vec::new(),
            wake_time: None,
            heap: Heap::new(),
        };
        for _ in 0..NUM_REGISTERS {
            ivm.registers.push(Register::new());
        }
        ivm
    }

    pub fn executable(&self) -> bool {
        match self.binary {
            Some(_) => true,
            None => false,
        }
    }

    pub fn start(&mut self) {
        self.active = true;
    }

    pub fn pause(&mut self) {
        self.active = false;
    }

    pub fn set_eax(&mut self, v: i64) {
        self.registers[254].value = v;
    }

    pub fn set_ebx(&mut self, v: i64) {
        self.registers[253].value = v;
    }

    pub fn execute_one(&mut self) -> bool {
        // If the program counter is higher than the length of the binary,
        // we've overflowed and need to return

        if self.binary.is_none() {
            println!("Binary is none");
            return true;
        }

        if self.pc >= self.binary.as_ref().unwrap().code_length() as u64 {
            println!("Program counter is >= binary length!");
            return false;
        }

        // Gets the next byte, which should be the next opcode at this point
        let next_byte = self.binary.as_ref().unwrap().byte_at(self.pc);
        if next_byte.is_none() {
            println!("Next byte is none!");
            return false;
        }

        // Converts the next byte to an opcode
        let opcode = int_to_opcode(next_byte.unwrap());
        if opcode.is_none() {
            println!("Opcode is none!");
            return false;
        }

        // Increment program counter since we've found the next opcode
        self.pc += 1;

        match opcode.unwrap() {
            Opcode::END => {
                return false;
            }
            Opcode::LOD => {
                // LOD takes two operands, the target register and the value
                let register_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let mut register = &mut self.registers[register_byte as usize];
                self.pc += 1;

                // This is the value we want to load into the register
                // The max size we can load into a register is a 16-bit integer
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc, self.pc + 2);
                self.pc += 2;
                let value = Cursor::new(value.unwrap()).read_i16::<BigEndian>();


                // Set the value in the register
                register.value = value.unwrap() as i64;
                return true;
            }
            Opcode::ADD => {
                // ADDs two registers together and stores the result in the first
                // We use split_at_mut so we can have two separate borrows to two different
                // vector elements at once

                // Grab the destination register
                let destination_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                self.pc += 1;

                // Grab the first register
                let register1_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let v1 = self.registers[register1_byte as usize].value;
                self.pc += 1;

                // Grab the second
                let register2_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let v2 = self.registers[register2_byte as usize].value;
                self.pc += 1;

                let total = v1 + v2;
                self.registers[destination_byte as usize].value = total;
                return true;

            }
            Opcode::SUB => {
                // Subtracts the second register from the first
                // We use split_at_mut so we can have two separate borrows to two different
                // vector elements at once

                // Grab the first register
                let register1_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let (register1, rest) = self.registers
                    .split_at_mut(register1_byte as usize + 1);
                self.pc += 1;

                // Grab the second
                let register2_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let (register2, _) = rest.split_at_mut(register2_byte as usize);
                self.pc += 1;

                register1[0].value = register1[0].value - register2[0].value;
                return true;
            }
            Opcode::MUL => {
                // Multiplies two registers together and stores the result in the first
                // We use split_at_mut so we can have two separate borrows to two different
                // vector elements at once

                // Grab the first register
                let register1_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let (register1, rest) = self.registers
                    .split_at_mut(register1_byte as usize + 1);
                self.pc += 1;

                // Grab the second
                let register2_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let (register2, _) = rest.split_at_mut(register2_byte as usize);
                self.pc += 1;

                register1[0].value = register1[0].value * register2[0].value;
                return true;
            }
            Opcode::DIV => {
                // Divides the value in EAX by the operand and places the quotient in EAX and remainder in EDX
                // Get the operand

                let value = self.binary.as_ref().unwrap().bytes_at(self.pc, self.pc + 3);
                self.pc += 3;
                let value = Cursor::new(value.unwrap()).read_i24::<BigEndian>().unwrap();

                let quotient = self.registers[254].value / value as i64;
                let remainder = self.registers[254].value % value as i64;
                self.set_eax(quotient);
                self.set_ebx(remainder);
                return true;
            }
            Opcode::JMP => {
                // Executes an unconditional jump to an absolute value
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc, self.pc + 2);
                self.pc += 2;
                let value = (Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap()) * 4;
                self.pc = value as u64;
                return true;
            }
            Opcode::JMPR => {
                // Executes an unconditional jump relative to the current PC
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc + 1, self.pc + 3);
                self.pc += 3;
                let value = (Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap()) * 4;
                if value <= 0 {
                    self.pc = self.pc - value.abs() as u64;
                } else {
                    self.pc = self.pc + value as u64;
                }
                return true;
            }
            Opcode::JEQ => {
                // Executes a relative jump if the zf flag is zero
                if self.zf == 0 {
                    return true;
                }
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc + 1, self.pc + 3);
                self.pc += 3;
                let value = (Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap()) * 4;
                if value <= 0 {
                    self.pc = self.pc - value.abs() as u64;
                } else {
                    self.pc = self.pc + value as u64;
                }
                return true;
            }
            Opcode::JNEQ => {
                // Executes a relative jump if the zf flag is not zero
                if self.zf != 0 {
                    return true;
                }
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc, self.pc + 3);
                self.pc += 3;
                let value = Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap();
                if value <= 0 {
                    self.pc = self.pc - (value.abs() as u64 * 4);
                } else {

                    self.pc = self.pc + value as u64;
                }

                return true;
            }
            Opcode::LOOP => {
                return true;
            }
            Opcode::CMP => {
                // Compares two registers and sets
                let register1_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let register1_value = self.registers[register1_byte as usize].value;
                self.pc += 1;

                // Grab the second
                let register2_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let register2_value = self.registers[register2_byte as usize].value;
                self.pc += 1;


                if register1_value == register2_value {
                    self.zf = 1;
                    self.cf = 0;
                } else if register1_value < register2_value {
                    self.zf = 0;
                    self.cf = 1;
                } else if register1_value > register2_value {
                    self.zf = 0;
                    self.cf = 0;
                }
                self.pc += 1;
                return true;
            }
            Opcode::JZ => {
                if self.zf == 0 {
                    return true;
                }
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc + 1, self.pc + 3);
                self.pc += 3;
                let value = (Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap()) * 4;
                if value <= 0 {
                    self.pc = self.pc - value.abs() as u64;
                } else {
                    self.pc = self.pc + value as u64;
                }
                return true;
            }
            Opcode::JNZ => {
                if self.zf != 0 {
                    return true;
                }
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc + 1, self.pc + 3);
                self.pc += 3;
                let value = (Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap()) * 4;
                if value <= 0 {
                    self.pc = self.pc - value.abs() as u64;
                } else {
                    self.pc = self.pc + value as u64;
                }
                return true;
            }
            Opcode::PRT => {
                let register1_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let (_, rest) = self.registers
                    .split_at_mut(register1_byte as usize);
                print!("{:?}", rest[0].value);
                self.pc += 1;

                return true;
            }
            Opcode::SLP => {
                self.active = false;
                let now = time::Instant::now();
                let value = self.binary.as_ref().unwrap().bytes_at(self.pc, self.pc + 2);
                self.pc += 3;
                let value = Cursor::new(value.unwrap()).read_i16::<BigEndian>().unwrap();
                self.wake_time = Some(now + time::Duration::from_millis(value as u64));
                return true;
            }
            Opcode::IRO => {
                // Grab the type of IRO to allocate
                // 0: IRString
                // 1: Number
                // 2: Array
                // 3: Hash
                let type_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                let address: i64;
                self.pc += 1;

                match type_byte {
                    0 => address = self.heap.new_obj(IRType::IRString),
                    1 => address = self.heap.new_obj(IRType::Number),
                    2 => address = self.heap.new_obj(IRType::Array),
                    3 => address = self.heap.new_obj(IRType::Hash),
                    _ => address = -1,
                }

                let destination_byte = self.binary.as_ref().unwrap().byte_at(self.pc).unwrap();
                self.registers[destination_byte as usize].value = address;
                self.pc += 2;
                return true;
            }
            Opcode::LODS => {
                return true;
            }
            Opcode::LODN => {
                return true;
            }
            Opcode::LODA => {
                return true;
            }
            Opcode::LODH => {
                return true;
            }
            Opcode::PRTS => {
                // We need to get the address in the data section of the string we want to print
                let destination = self.binary.as_ref().unwrap().bytes_at(self.pc + 1, self.pc + 3);
                self.pc += 3;
                let destination = Cursor::new(destination.unwrap()).read_i16::<BigEndian>().unwrap() as u32;
                let data = self.binary.as_ref().unwrap().data.get(&destination).unwrap();

                // TODO: Can we do this without cloning?
                let data = String::from_utf8(data.clone()).unwrap();
                print!("{:?}", data);
                return true;
            }
        }
    }

    pub fn execute(&mut self) -> bool {
        let ten_millis = time::Duration::from_millis(10);
        // let now = time::Instant::now();

        let mut done = false;
        while !done {
            if self.active {
                done = self.execute_one();
            } else {
                thread::sleep(ten_millis);
            }
        }
        return done;
    }

    pub fn print_registers(&self) {
        println!("----Registers----");
        for (i, r) in self.registers.iter().enumerate() {
            println!("|\tRegister {:?}:\t{:?}", i, r);
        }
        println!("-----------------");
    }

    pub fn load_binary(&mut self, binary: Binary) {
        self.binary = Some(binary);
    }

    pub fn set_register(&mut self, register: usize, value: i64) {
        self.registers[register].value = value;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn create_test_end_binary() -> Binary {
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![0]);
        binary
    }

    fn create_test_lod_binary() -> Binary {
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![5, 0, 0, 1]);
        binary
    }

    #[test]
    fn create_ivm() {
        let ivm = IVM::new();
        let expected_result = IVM::new();
        assert_eq!(ivm, expected_result);
    }

    #[test]
    fn execute_end() {
        let mut ivm = IVM::new();
        ivm.load_binary(create_test_end_binary());
        let result = ivm.execute_one();
        assert_eq!(false, result);
    }

    #[test]
    fn execute_lod() {
        let mut ivm = IVM::new();
        ivm.load_binary(create_test_lod_binary());
        ivm.execute_one();
        assert_eq!(ivm.registers[0].value, 1);
    }

    #[test]
    fn execute_lod_special() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![5, 254, 0, 1]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.registers[254].value, 1);
    }

    #[test]
    fn execute_lod_negative() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![5, 0, 255, 246]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.registers[0].value, -10);

    }

    #[test]
    fn execute_add() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![1, 0, 1, 0]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.registers[0].value, 0);
    }

    #[test]
    fn execute_mul() {
        let mut ivm = IVM::new();
        ivm.set_register(0, 5);
        ivm.set_register(1, 5);
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![3, 0, 1, 0]);

        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.registers[0].value, 25);
    }

    #[test]
    fn execute_sub() {
        let mut ivm = IVM::new();
        ivm.set_register(0, 10);
        ivm.set_register(1, 5);
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![2, 0, 1, 0]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.registers[0].value, 5);
    }

    #[test]
    fn execute_div() {
        let mut ivm = IVM::new();
        ivm.registers[254].value = 11;
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![4, 0, 0, 5]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.registers[254].value, 2);
        assert_eq!(ivm.registers[253].value, 1);
    }

    #[test]
    fn execute_jmp() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![5, 0, 0, 1]);
        binary.add_code(vec![6, 0, 0, 0]);
        ivm.load_binary(binary);
        ivm.execute_one();
        ivm.execute_one();
        assert_eq!(ivm.pc, 0);
    }

    #[test]
    fn execute_jmpr_back() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![5, 0, 0, 1]);
        binary.add_code(vec![7, 0, 255, 255]);
        ivm.load_binary(binary);
        ivm.execute_one();
        ivm.execute_one();
        assert_eq!(ivm.pc, 4);
    }

    #[test]
    fn execute_jmpr_fwd() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![5, 0, 0, 1]);
        binary.add_code(vec![7, 0, 0, 1]);
        ivm.load_binary(binary);
        ivm.execute_one();
        ivm.execute_one();
        assert_eq!(ivm.pc, 12);
    }

    #[test]
    fn execute_slp() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![14, 0, 0, 1]);
        binary.add_code(vec![6, 0, 0, 0]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.active, false);
    }

    #[test]
    fn execute_iro() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.add_code(vec![16, 0, 0, 1]);
        ivm.load_binary(binary);
        ivm.execute_one();
        assert_eq!(ivm.heap.counter, 1);
    }

    #[test]
    fn execute_prts() {
        let mut ivm = IVM::new();
        let mut binary = Binary::new(0.1);
        binary.data.insert(0, vec![84, 101, 115, 116, 32, 115, 116, 114, 105, 110, 103]);
        binary.add_code(vec![21, 0, 0, 0]);
        ivm.load_binary(binary);
        ivm.execute_one();
    }
}
