/// Allocates registers as it compiles to assembly
pub struct Allocator {
    registers: Vec<usize>,
}

impl Allocator {
    /// Creates and returns a new Register Allocator
    pub fn new() -> Allocator {
        let mut new_allocator = Allocator { registers: Vec::new() };
        for i in 0..200 {
            new_allocator.registers.push(i);
        }
        new_allocator
    }

    /// allocates a free register
    pub fn allocate(&mut self) -> Option<usize> {
        self.registers.pop()
    }

    // deallocates a register, allowing it to be used again
    pub fn deallocate(&mut self, r: usize) {
        self.registers.push(r);
    }
}