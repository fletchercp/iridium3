use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;
use iris::compiler::object::*;
use iris::compiler::builtins::*;
use iris::parser::ast::*;

#[derive(PartialEq, Debug, Clone)]
pub struct Environment {
    store: HashMap<String, Object>,
    parent: Option<Rc<RefCell<Environment>>>,
    register_store: HashMap<String, usize>,
}

impl Default for Environment {
    fn default() -> Self {
        Self::new()
    }
}

impl Environment {
    pub fn new() -> Self {
        let mut hashmap = HashMap::new();
        Self::fill_env_with_builtins(&mut hashmap);
        Environment {
            store: hashmap,
            parent: None,
            register_store: HashMap::new(),
        }
    }

    pub fn new_with_outer(outer: Rc<RefCell<Environment>>) -> Self {
        let mut hashmap = HashMap::new();
        Self::fill_env_with_builtins(&mut hashmap);
        Environment {
            store: hashmap,
            parent: Some(outer),
            register_store: HashMap::new(),
        }
    }

    fn fill_env_with_builtins(hashmap: &mut HashMap<String, Object>) {
        let builtins_functions = BuiltinsFunctions::new();
        let builtins = builtins_functions.get_builtins();
        for (ident, object) in builtins {
            let Ident(name) = ident.clone();
            hashmap.insert(name.clone(), object);
        }
    }

    /// Maps a variable to a register in the current environment
    pub fn set_variable(&mut self, name: &str, val: usize) -> () {
        self.register_store.insert(name.to_string(), val);
    }

    /// Returns the register in which a variable is stored
    pub fn get_variable(&self, name: &str) -> Option<usize> {
        match self.register_store.get(name) {
            Some(&ref o) => Some(o.clone()),
            None => {
                match self.parent {
                    Some(ref parent_env) => {
                        let env = parent_env.borrow();
                        env.get_variable(name)
                    }
                    None => None,
                }
            }
        }
    }

    pub fn set(&mut self, name: &str, val: &Object) -> () {
        let val = val.clone();
        self.store.insert(name.to_string(), val);
    }

    pub fn get(&self, name: &str) -> Option<Object> {
        match self.store.get(name) {
            Some(&ref o) => Some(o.clone()),
            None => {
                match self.parent {
                    Some(ref parent_env) => {
                        let env = parent_env.borrow();
                        env.get(name)
                    }
                    None => None,
                }
            }
        }
    }
}