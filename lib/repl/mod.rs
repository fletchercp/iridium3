use std::fs::File;
use std::io::prelude::*;
use std::process::exit;

use vm;
use iasm::lexer::Lexer;
use iasm::parser::Parser;
use iasm::lexer::token::Tokens;
use iasm::assembler::IASM;

pub struct Repl {
    scheduler: vm::ivm::scheduler::Scheduler,
}

impl Repl {
    pub fn new() -> Repl {
        Repl{
            scheduler: vm::ivm::scheduler::Scheduler::new(),
        }
    }

    pub fn process(&mut self, input: &str) {
        let commands: Vec<&str> = input.split(" ").collect();
        match commands[0] {
            "quit" => {
                println!("Goodbye!");
                exit(0);
            }
            "spawn" => {
                let new_ivm = vm::ivm::IVM::new();
                self.scheduler.add_ivm(new_ivm);
                self.scheduler.spawn();
            }
            "load_iasm" => {
                if commands.len() != 2 {
                    println!("Please provide a path to a file to read!");
                    return;
                }
                let file = File::open(commands[1]);
                match file {
                    Ok(mut f) => {
                        let mut contents = String::new();
                        let result = f.read_to_string(&mut contents);
                        match result {
                            Ok(_) => {
                                let lexed_results = Lexer::lex_tokens(contents.as_bytes()).to_result().unwrap();
                                let tokens = Tokens::new(&lexed_results);
                                let program = Parser::parse_tokens(tokens).to_result();
                                let mut binary = IASM::new();
                                binary.assemble(program.unwrap());
                                let mut new_ivm = vm::ivm::IVM::new();
                                new_ivm.load_binary(binary.clone_binary().unwrap());
                                self.scheduler.add_ivm(new_ivm);
                            }
                            Err(e) => {
                                println!("Error reading file: {:?}", e);
                            }
                        }
                    }
                    Err(e) => {
                        println!("Error opening file: {:?}", e);
                    }
                }
            }
            _ => {
                println!("Command not recognized: {:?}", input);
            }
        }
    }
}
