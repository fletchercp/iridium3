use std::collections::HashMap;
use byteorder::{BigEndian, WriteBytesExt};

pub type Program = Vec<Instruction>;

#[derive(PartialEq, Debug, Clone)]
pub enum Instruction {
    OpcodeInst {
        label: Option<Label>,
        opcode: Opcode,
        operands: Vec<Operand>,
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum Opcode {
    END,
    LOD,
    ADD,
    SUB,
    MUL,
    DIV,
    JMP,
    JMPR,
    JEQ,
    JNEQ,
    LOOP,
    CMP,
    JZ,
    JNZ,
    SLP,
    PRT,
    IRO,
    LODS,
    LODN,
    LODA,
    LODH,
    PRTS,
}

#[derive(PartialEq, Debug, Clone)]
pub enum Directive {
    ASCII,
}


pub fn int_to_opcode(i: u8) -> Option<Opcode> {
    match i {
        0 => Some(Opcode::END),
        1 => Some(Opcode::ADD),
        2 => Some(Opcode::SUB),
        3 => Some(Opcode::MUL),
        4 => Some(Opcode::DIV),
        5 => Some(Opcode::LOD),
        6 => Some(Opcode::JMP),
        7 => Some(Opcode::JMPR),
        8 => Some(Opcode::JEQ),
        9 => Some(Opcode::JNEQ),
        10 => Some(Opcode::LOOP),
        11 => Some(Opcode::CMP),
        12 => Some(Opcode::JZ),
        13 => Some(Opcode::JNZ),
        14 => Some(Opcode::SLP),
        15 => Some(Opcode::PRT),
        16 => Some(Opcode::IRO),
        17 => Some(Opcode::LODS),
        18 => Some(Opcode::LODN),
        19 => Some(Opcode::LODA),
        20 => Some(Opcode::LODH),
        21 => Some(Opcode::PRTS),
        _ => None,
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum Operand {
    Register(i64),
    IntLiteral(i64),
    StringLiteral(String),
    SpecialRegister(String),
}

#[derive(PartialEq, Debug, Clone)]
pub struct Label {
    pub name: Option<String>,
    pub position: Option<i64>,
}

pub trait ASTNode {
    fn write(&self,
             labels: &mut HashMap<String, i32>,
             data: &mut HashMap<u32, Vec<u8>>,
             pass: usize)
             -> (Option<String>, Vec<u8>);
}

impl ASTNode for Instruction {
    fn write(&self,
             labels: &mut HashMap<String, i32>,
             data: &mut HashMap<u32, Vec<u8>>,
             pass: usize)
             -> (Option<String>, Vec<u8>) {

        let mut result: Vec<u8> = Vec::new();
        let mut ret_label: Option<String> = None;

        // During the first pass of the assembler we want to do two things:
        // 1) Look for labels and replace them with the actual count
        // 2) Look for instructions that load things directly into the data segment,
        //    such as strings and put them into the data section
        // 3) Look for instructions that reference things in the data segment by a logical name
        //    and replace them with the address in the data segment
        if pass == 1 {
            match self {
                &Instruction::OpcodeInst { ref label, ref opcode, ref operands } => {
                    // First check to see if there was a label
                    match label {
                        // We have a label, so we'll make sure to return it
                        &Some(ref l) => {
                            println!("Found a label in {:?} in first pass...", self);
                            ret_label = l.name.clone();
                        }
                        // No label, don't need to do anything
                        &None => {}
                    };

                    // Next we need to check to see if the instruction is one of the ones that
                    // will load something into the data segment
                    match opcode {
                        // Handle the case of loading a string into memory and associating it with
                        // a logical name
                        // LODS should follow the following format:
                        // LODS name "string"
                        // Example: LODS test "Test string"
                        &Opcode::LODS => {
                            println!("Processing LODS in first pass of assembler...");
                            let key: String;
                            let value: String;
                            let address = data.keys().len() as u32;

                            match &operands[0] {
                                &Operand::StringLiteral(ref s) => {
                                    key = s.clone();
                                },
                                _ => {
                                    return (None, vec![]);
                                }
                            };

                            match &operands[1] {
                                &Operand::StringLiteral(ref s) => {
                                    value = s.clone();
                                },
                                _ => { 
                                    return (None, vec![]);
                                }
                            };

                            labels.insert(key, address as i32);
                            data.insert(address, value.into_bytes());
                            return (None, vec![]);
                        },
                        _ => {
                            println!("Ignoring instruction {:?} in first pass...", self);
                            let needed = 4 - result.len();
                            for _ in 0..needed {
                                result.push(0);
                            }
                            println!("Returning {:?} {:?}", ret_label, result);
                            return (ret_label, result);
                        },
                    }
                }
            }

        // During the second pass, we want to actually write the opcode and operand bytes
        } else if pass == 2 {
            match self {
                &Instruction::OpcodeInst { ref label, ref opcode, ref operands } => {
                    println!("Processing opcode {:?} in second pass...", self);
                    // Get the opcode data as bytes
                    let (_, opcode_data) = opcode.write(labels, data, pass);
                    // Add the opcode bytes to the bytes to return
                    result.extend(opcode_data);
                    // Need to handle special opcodes where we want to replace logical names in the
                    // data section with the addresses
                    match *opcode {
                        // PRTS example:
                        // PRTS test
                        // We need to replace test with the actual address in data hash, which means
                        // we need to look it up the label hash
                        Opcode::PRTS => {
                            match operands[0] {
                                Operand::StringLiteral(ref s) => {
                                    println!("Need to convert {:?} to address", s);
                                    let address = labels.get(s).unwrap();
                                    let mut wtr = vec![];
                                    wtr.write_i16::<BigEndian>(*address as i16).unwrap();
                                    result.extend(wtr);
                                },
                                _ => {},
                            };
                        }
                        // JMP jumps to a specific destination
                        Opcode::JMP => {
                            match operands[0] {
                                Operand::StringLiteral(ref s) => {
                                    println!("Need to convert {:?} to address", s);
                                    let address = labels.get(s).unwrap();
                                    let mut wtr = vec![];
                                    wtr.write_i16::<BigEndian>(*address as i16).unwrap();
                                    result.extend(wtr);
                                }
                                _ => {
                                    // TODO: Need to throw an error here?
                                }
                            }
                        }
                        // We want to ignore LODS instructions in the second pass
                        // TODO: We should remove them completely in the first pass, or something...
                        Opcode::LODS => {
                            return (None, vec![]);
                        }
                        // Handle all non-special opcodes, that is, ones we don't need to do looks up in the
                        // data segment for
                        _ => {
                            // Need to iterate over each operand and transfer them into appropriate bytes to
                            // return
                            for operand in operands {
                                println!("Processing operand: {:?}", operand);
                                // Get the bytes of the operand
                                let (label, operand_data) = operand.write(labels, data, pass);
                                // If there was a label, we want to replace the label with the destination
                                // bytes
                                if label.is_some() {
                                    println!("Found a label we need to look up: {:?}", label);
                                    let destination = labels.get(&label.unwrap());
                                    if destination.is_some() {
                                        let mut wtr = vec![];
                                        wtr.write_i16::<BigEndian>(*destination.unwrap() as i16).unwrap();
                                        result.extend(wtr);
                                    }
                                }
                                // Add the final operand data to the array to return
                                result.extend(operand_data);
                            }
                        }
                    };
                }
            }
        } else {
            // We should never get here since pass should always be 1 or 2
            return (None, vec![]);
        }

        // We need to pad the result since each instruction has to be 32 bits, even if there is not enough
        // bits in the opcode + operands to total 32 bits
        let needed = 4 - result.len();
        for _ in 0..needed {
            result.push(0);
        }
        (ret_label, result)
    }
}

impl ASTNode for Opcode {
    fn write(&self,
             _: &mut HashMap<String, i32>,
             _: &mut HashMap<u32, Vec<u8>>,
             _: usize)
             -> (Option<String>, Vec<u8>) {
        let mut wtr = vec![];
        match self {
            &Opcode::END => {
                wtr.write_u8(0).unwrap();
            }
            &Opcode::ADD => {
                wtr.write_u8(1).unwrap();
            }
            &Opcode::SUB => {
                wtr.write_u8(2).unwrap();
            }
            &Opcode::MUL => {
                wtr.write_u8(3).unwrap();
            }
            &Opcode::DIV => {
                wtr.write_u8(4).unwrap();
            }
            &Opcode::LOD => {
                wtr.write_u8(5).unwrap();
            }
            &Opcode::JMP => {
                wtr.write_u8(6).unwrap();
            }
            &Opcode::JMPR => {
                wtr.write_u8(7).unwrap();
            }
            &Opcode::JEQ => {
                wtr.write_u8(8).unwrap();
            }
            &Opcode::JNEQ => {
                wtr.write_u8(9).unwrap();
            }
            &Opcode::LOOP => {
                wtr.write_u8(10).unwrap();
            }
            &Opcode::CMP => {
                wtr.write_u8(11).unwrap();
            }
            &Opcode::JZ => {
                wtr.write_u8(12).unwrap();
            }
            &Opcode::JNZ => {
                wtr.write_u8(13).unwrap();
            }
            &Opcode::SLP => {
                wtr.write_u8(14).unwrap();
            }
            &Opcode::PRT => {
                wtr.write_u8(15).unwrap();
            }
            &Opcode::IRO => {
                wtr.write_u8(16).unwrap();
            }
            &Opcode::LODS => {
                wtr.write_u8(17).unwrap();
            }
            &Opcode::LODN => {
                wtr.write_u8(18).unwrap();
            }
            &Opcode::LODA => {
                wtr.write_u8(19).unwrap();
            }
            &Opcode::LODH => {
                wtr.write_u8(20).unwrap();
            }
            &Opcode::PRTS => {
                wtr.write_u8(21).unwrap();
            }
        }
        (None, wtr)
    }
}

impl ASTNode for Operand {
    fn write(&self,
             _: &mut HashMap<String, i32>,
             _: &mut HashMap<u32, Vec<u8>>,
             _: usize)
             -> (Option<String>, Vec<u8>) {
        match self {
            &Operand::Register(n) => {
                let mut wtr = vec![];
                wtr.write_u8(n as u8).unwrap();
                (None, wtr)
            }
            &Operand::SpecialRegister(ref s) => {
                let mut wtr = vec![];
                match s.as_ref() {
                    "eax" => wtr.write_u8(255 as u8).unwrap(),
                    "ebx" => wtr.write_u8(254 as u8).unwrap(),
                    &_ => {}
                }
                (None, wtr)
            }
            &Operand::IntLiteral(n) => {
                let mut wtr = vec![];
                wtr.write_i16::<BigEndian>(n as i16).unwrap();
                (None, wtr)
            }
            &Operand::StringLiteral(ref s) => {
                let mut wtr = vec![];
                for c in s.chars() {
                    wtr.write_u8(c as u8).unwrap();
                }
                wtr.write_u8(0).unwrap();
                (None, wtr)
            }
        }
    }
}

impl ASTNode for Label {
    fn write(&self,
             _: &mut HashMap<String, i32>,
             _: &mut HashMap<u32, Vec<u8>>,
             _: usize)
             -> (Option<String>, Vec<u8>) {
        (None, vec![0])
    }
}
