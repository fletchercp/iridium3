use nom::*;

pub mod ast;
use iasm::lexer::token::*;
use iasm::parser::ast::*;

macro_rules! tag_token (
  ($i: expr, $tag: expr) => (
    {
        let (i1, t1) = try_parse!($i, take!(1));
        if t1.tok.is_empty() {
            IResult::Incomplete::<_,_,u32>(Needed::Size(1))
        } else {
            if t1.tok[0] == $tag {
                IResult::Done(i1, t1)
            } else {
                IResult::Error(error_position!(ErrorKind::Count, $i))
            }
        }
    }
  );
);

macro_rules! parse_opcode (
  ($i: expr,) => (
    {
        let (i1, t1) = try_parse!($i, take!(1));
        if t1.tok.is_empty() {
            IResult::Error(error_position!(ErrorKind::Tag, $i))
        } else {
            match t1.tok[0].clone() {
                Token::Opcode(name) => {
                    match name.as_ref() {
                        "END" => IResult::Done(i1, Opcode::END),
                        "ADD" => IResult::Done(i1, Opcode::ADD),
                        "SUB" => IResult::Done(i1, Opcode::SUB),
                        "DIV" => IResult::Done(i1, Opcode::DIV),
                        "MUL" => IResult::Done(i1, Opcode::MUL),
                        "LOD" => IResult::Done(i1, Opcode::LOD),
                        "JMP" => IResult::Done(i1, Opcode::JMP),
                        "PRT" => IResult::Done(i1, Opcode::PRT),
                        "JMPR" => IResult::Done(i1, Opcode::JMPR),
                        "JEQ" => IResult::Done(i1, Opcode::JEQ),
                        "JNEQ" => IResult::Done(i1, Opcode::JNEQ),
                        "CMP" => IResult::Done(i1, Opcode::CMP),
                        "JZ" => IResult::Done(i1, Opcode::JZ),
                        "JNZ" => IResult::Done(i1, Opcode::JNZ),
                        "SLP" => IResult::Done(i1, Opcode::SLP),
                        "IRO" => IResult::Done(i1, Opcode::IRO),
                        "LODS" => IResult::Done(i1, Opcode::LODS),
                        "LODN" => IResult::Done(i1, Opcode::LODN),
                        "LODA" => IResult::Done(i1, Opcode::LODA),
                        "LODH" => IResult::Done(i1, Opcode::LODH),
                        "PRTS" => IResult::Done(i1, Opcode::PRTS),
                        _ => IResult::Error(error_position!(ErrorKind::Tag, $i)),
                    }
                },
                _ => IResult::Error(error_position!(ErrorKind::Tag, $i)),
            }
        }
    }
  );
);

macro_rules! parse_operand (
  ($i: expr,) => (
    {
        let (i1, t1) = try_parse!($i, take!(1));
        if t1.tok.is_empty() {
            IResult::Incomplete::<_,_,u32>(Needed::Size(1))
        } else {
            match t1.tok[0].clone() {
                Token::Register(rid) => IResult::Done(i1, Operand::Register(rid)),
                Token::IntLiteral(num) => IResult::Done(i1, Operand::IntLiteral(num)),
                Token::StringLiteral(s) => IResult::Done(i1, Operand::StringLiteral(s)),
                Token::SpecialRegister(s) => IResult::Done(i1, Operand::SpecialRegister(s)),
                Token::Negative => {
                    let (i2, t2) = try_parse!($i, take!(2));
                    match t2.tok[1].clone() {
                        Token::IntLiteral(num) => IResult::Done(i2, Operand::IntLiteral(-num)),
                        _ => IResult::Error(error_position!(ErrorKind::Tag, $i)),
                    }
                }
                _ => IResult::Error(error_position!(ErrorKind::Tag, $i)),
            }
        }
    }
  );
);

macro_rules! parse_label (
  ($i: expr,) => (
    {
        let (i1, t1) = try_parse!($i, take!(1));
        if t1.tok.is_empty() {
            IResult::Incomplete::<_,_,u32>(Needed::Size(1))
        } else {
            match t1.tok[0].clone() {
                Token::Label{label} => IResult::Done(i1, Label{name: Some(label), position: Some(0)}),
                _ => IResult::Error(error_position!(ErrorKind::Tag, $i)),
            }
        }
    }
  );
);

named!(parse_program<Tokens, Program>,
    do_parse!(
        prog: many0!(parse_instruction) >>
        tag_token!(Token::EOF) >>
        (prog)
    )
);

named!(parse_instruction<Tokens, Instruction>,
    do_parse!(
        l: opt!(parse_label!()) >>
        opcode: parse_opcode!() >>
        operands: many0!(parse_operand!()) >>
        (Instruction::OpcodeInst { label: l, opcode: opcode, operands: operands})
    )
);

pub struct Parser {}

impl Parser {
    pub fn parse_tokens(tokens: Tokens) -> IResult<Tokens, Program> {
        let p = parse_program(tokens);
        p
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use iasm::lexer::*;

    fn assert_input_with_program(input: &[u8], expected_results: Program) {
        let r = Lexer::lex_tokens(input).to_result().unwrap();
        let tokens = Tokens::new(&r);
        let result = Parser::parse_tokens(tokens).to_result();
        assert_eq!(result.unwrap(), expected_results);
    }

    #[test]
    fn empty() {
        assert_input_with_program(&b""[..], vec![]);
    }

    #[test]
    fn operand_registers() {
        let input = "ADD r0 r2".as_bytes();
        let program: Program =
        vec![
            Instruction::OpcodeInst{ label: None, opcode: Opcode::ADD, operands: vec![Operand::Register(0), Operand::Register(2)] }
        ];

        assert_input_with_program(input, program);
    }

    #[test]
    fn operand_integers() {
        let input = "LOD 10 r0".as_bytes();
        let program: Program =
        vec![
            Instruction::OpcodeInst{ label: None, opcode: Opcode::LOD, operands: vec![Operand::IntLiteral(10), Operand::Register(0)] }
        ];

        assert_input_with_program(input, program);
    }

    #[test]
    fn lod_special_register() {
        let input = "LOD 10 ebx".as_bytes();
        let program: Program =
        vec![
            Instruction::OpcodeInst{ label: None, opcode: Opcode::LOD, operands: vec![Operand::IntLiteral(10), Operand::SpecialRegister(String::from("ebx"))] }
        ];
        assert_input_with_program(input, program);
    }

    #[test]
    fn labels() {
        let input = "test: LOD 10 r0\nJMP \"test\"".as_bytes();
        let program: Program =
        vec![
            Instruction::OpcodeInst{ label: Some(Label{name: Some("test".to_string()), position: Some(0)}), opcode: Opcode::LOD, operands: vec![Operand::IntLiteral(10), Operand::Register(0)] },
            Instruction::OpcodeInst{ label: None, opcode: Opcode::JMP, operands: vec![Operand::StringLiteral(String::from("test"))] },
        ];
        assert_input_with_program(input, program);
    }

    #[test]
    fn negative() {
        let input = "LOD -10 r0".as_bytes();
        let program: Program =
        vec![
            Instruction::OpcodeInst{ label: None, opcode: Opcode::LOD, operands: vec![Operand::IntLiteral(-10), Operand::Register(0)] }
        ];
        assert_input_with_program(input, program);
    }
    #[test]
    fn lods() {
        let input = "LODS \"Test string\"".as_bytes();
        let program: Program =
        vec![
            Instruction::OpcodeInst{ label: None, opcode: Opcode::LODS, operands: vec![Operand::StringLiteral(String::from("Test string"))] }
        ];
        assert_input_with_program(input, program);
    }
}
