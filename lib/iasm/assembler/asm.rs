use std::collections::HashMap;


#[derive(PartialEq, Debug, Clone)]
pub struct Binary {
    version: f64,
    code: Vec<u8>,
    pub labels: HashMap<String, i32>,
    pub data: HashMap<u32, Vec<u8>>,
}

impl Binary {
    pub fn new(version: f64) -> Binary {
        Binary {
            version: version,
            code: Vec::new(),
            labels: HashMap::new(),
            data: HashMap::new(),
        }
    }

    pub fn add_code(&mut self, code: Vec<u8>) -> bool {
        self.code.extend(code);
        true
    }

    pub fn print(&self) {
        println!("{:?}", self.code);
    }

    pub fn print_data(&self) {
        println!("{:?}", self.data);
    }

    pub fn code_length(&self) -> usize {
        self.code.len()
    }

    pub fn byte_at(&self, offset: u64) -> Option<u8> {
        if offset as usize > self.code.len() {
            return None;
        }
        Some(self.code[offset as usize].clone())
    }

    pub fn bytes_at(&self, start: u64, stop: u64) -> Option<Vec<u8>> {
        if stop as usize > self.code.len() {
            return None;
        }
        let mut results: Vec<u8> = Vec::new();
        for b in self.code[start as usize..stop as usize].as_ref() {
            results.push(b.clone());
        }
        Some(results)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_binary() {
        let b = Binary::new(0.1);
        let expected_result = Binary {
            version: 0.1,
            code: Vec::<u8>::new(),
            labels: HashMap::new(),
            data: HashMap::new(),
            
        };
        assert_eq!(b, expected_result);
    }

    #[test]
    fn add_code() {
        let mut b = Binary::new(0.1);
        b.add_code(vec![1]);
        let expected_result = Binary {
            version: 0.1,
            code: vec![1],
            labels: HashMap::new(),
            data: HashMap::new(),
        };
        assert_eq!(b, expected_result);
    }

    #[test]
    fn write_code() {
        // let mut b = Binary::new(0.1);
        // let i = create_test_inst();
    }
}