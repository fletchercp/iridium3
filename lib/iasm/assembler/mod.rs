use std::collections::HashMap;

pub mod asm;
use iasm::assembler::asm::*;
use iasm::parser::ast;

use iasm::parser::ast::ASTNode;

#[derive(PartialEq, Debug, Clone)]
pub struct IASM {
    pub counter: i32,
    binary: Option<Binary>,
    labels: HashMap<String, i32>,
    pass: usize,
}

impl IASM {
    pub fn new() -> IASM {
        IASM {
            counter: 0,
            binary: None,
            labels: HashMap::new(),
            pass: 1,
        }
    }

    pub fn print_binary(&mut self) {
        self.binary.as_ref().unwrap().print();
    }

    pub fn print_labels(&self) {
        println!("{:?}", self.labels);
    }

    pub fn print_data(&self) {
        self.binary.as_ref().unwrap().print_data();
    }

    pub fn load_binary(&mut self, b: Binary) -> bool {
        self.binary = Some(b);
        true
    }

    pub fn clone_binary(&self) -> Option<Binary> {
        self.binary.clone()
    }

    pub fn assemble(&mut self, p: ast::Program) -> bool {
        if self.binary.is_none() {
            self.binary = Some(Binary::new(0.1));
        };
        let mut b = self.binary.as_mut().unwrap();
        // First pass to build the labels hashmap
        for instruction in p.clone() {
            let (label, val) = instruction.write(&mut self.labels, &mut b.data, self.pass);
            if label.is_some() && val.len() > 0 {
                self.labels.insert(label.unwrap(), self.counter as i32);
            }
            self.counter += 1;
        }
        self.counter = 0;
        println!("Beginning second pass of assembler...");
        self.pass = 2;
        // Second pass to generate the code
        for instruction in p {
            let (_, val) = instruction.write(&mut self.labels, &mut b.data, self.pass);
            b.add_code(val);
            self.counter += 1;
        }
        b.labels = self.labels.clone();
        true
    }

    pub fn binary_length(&mut self) -> usize {
        if self.binary.is_none() {
            return 0;
        };
        self.binary.as_mut().unwrap().code_length()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn create_test_program() -> ast::Program {
        let inst = ast::Instruction::OpcodeInst {
            label: None,
            opcode: ast::Opcode::ADD,
            operands: vec![ast::Operand::Register(0), ast::Operand::Register(1)],
        };
        let mut p: ast::Program = ast::Program::new();
        p.push(inst);
        p
    }

    fn create_test_loop_program() -> ast::Program {
        let inst = ast::Instruction::OpcodeInst {
            label: Some(ast::Label {
                name: Some(String::from("test")),
                position: Some(0),
            }),
            opcode: ast::Opcode::ADD,
            operands: vec![ast::Operand::Register(0), ast::Operand::Register(1)],
        };
        let mut p: ast::Program = ast::Program::new();
        p.push(inst);
        p
    }

    #[test]
    fn create_iasm() {
        let asm = IASM::new();
        let expected_result = IASM::new();
        assert_eq!(asm, expected_result);
    }

    #[test]
    fn assemble() {
        let mut asm = IASM::new();
        let p = create_test_program();
        asm.assemble(p);
        assert_eq!(asm.binary_length(), 4);
    }

    #[test]
    fn assemble_label() {
        let mut asm = IASM::new();
        let p = create_test_loop_program();
        asm.assemble(p);
        assert_eq!(asm.binary_length(), 4);
        assert_eq!(asm.labels.get("test").unwrap(), &0);
    }

    #[test]
    fn assemble_labels() {
        let mut asm = IASM::new();
        let inst = ast::Instruction::OpcodeInst {
            label: Some(ast::Label {
                name: Some(String::from("test")),
                position: Some(0),
            }),
            opcode: ast::Opcode::LOD,
            operands: vec![ast::Operand::Register(0), ast::Operand::IntLiteral(10)],
        };
        let inst2 = ast::Instruction::OpcodeInst {
            label: None,
            opcode: ast::Opcode::JMP,
            operands: vec![ast::Operand::StringLiteral(String::from("test"))],
        };
        let mut p: ast::Program = ast::Program::new();
        p.push(inst);
        p.push(inst2);
        asm.assemble(p);
    }

    #[test]
    fn assemble_strings() {
        let mut asm = IASM::new();
        let inst = ast::Instruction::OpcodeInst {
            label: None,
            opcode: ast::Opcode::LODS,
            operands: vec![
                ast::Operand::StringLiteral(String::from("test1")),
                ast::Operand::StringLiteral(String::from("Test string")),
            ],
        };
        let mut p: ast::Program = ast::Program::new();
        p.push(inst);
        asm.assemble(p);
        asm.print_data();
    }

    #[test]
    fn assemble_prts() {
        let mut asm = IASM::new();
        let inst = ast::Instruction::OpcodeInst {
            label: None,
            opcode: ast::Opcode::LODS,
            operands: vec![
                ast::Operand::StringLiteral(String::from("test1")),
                ast::Operand::StringLiteral(String::from("Test string")),
            ],
        };
        let inst2 = ast::Instruction::OpcodeInst {
            label: None,
            opcode: ast::Opcode::PRTS,
            operands: vec![
                ast::Operand::StringLiteral(String::from("test1")),
            ],
        };
        let mut p: ast::Program = ast::Program::new();
        p.push(inst);
        p.push(inst2);
        asm.assemble(p);
        assert_eq!(asm.binary_length(), 4);
    }

}
