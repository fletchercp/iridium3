use nom::*;
use std::str;
use std::str::FromStr;
use std::str::{Utf8Error, from_utf8};
pub mod token;
use iasm::lexer::token::*;

// punctuations
named!(colon_punctuation<&[u8], Token>,
    do_parse!(tag!(":") >> (Token::Colon))
);

named!(semicolon_punctuation<&[u8], Token>,
    do_parse!(tag!(";") >> (Token::SemiColon))
);

named!(lex_punctuations<&[u8], Token>, alt!(
    semicolon_punctuation |
    colon_punctuation
    )
);

// registers
named!(register_start, tag!("r"));

named!(register_special,
    alt!( tag!("eax") | tag!("ebx") )
);

named!(lex_data_register<&[u8], Token>,
    do_parse!(
        register_start >> 
        i: opt!(map_res!(map_res!(digit, str::from_utf8), FromStr::from_str)) >>
        (Token::Register(i.unwrap()))
    )
);

named!(lex_special_register<&[u8], Token>,
    do_parse!(
        i: register_special >>
        (Token::SpecialRegister(String::from(str::from_utf8(i).unwrap())))
    )
);

named!(lex_register<&[u8], Token>,
    alt_complete!(
        lex_data_register |
        lex_special_register
    )
);

// labels
named!(alphachars<&str>,
    map_res!(take_while!(is_alphabetic), from_utf8)
);

named!(lex_labels<&[u8], Token>,
    do_parse!(
        l: alphachars >>
        colon_punctuation >>
        (Token::Label{ label: l.to_string() })
    )
);

// strings
fn pis(input: &[u8]) -> IResult<&[u8], Vec<u8>> {
    let (i1, c1) = try_parse!(input, take!(1));
    match c1 {
        b"\"" => IResult::Done(input, vec![]),
        b"\\" => {
            let (i2, c2) = try_parse!(i1, take!(1));
            pis(i2).map(|done| concat_slice_vec(c2, done))
        }
        c => pis(i1).map(|done| concat_slice_vec(c, done)),
    }
}

fn concat_slice_vec(c: &[u8], done: Vec<u8>) -> Vec<u8> {
    let mut new_vec = c.to_vec();
    new_vec.extend(&done);
    new_vec
}

fn convert_vec_utf8(v: Vec<u8>) -> Result<String, Utf8Error> {
    let slice = v.as_slice();
    str::from_utf8(slice).map(|s| s.to_owned())
}

named!(string<String>,
  delimited!(
    tag!("\""),
    map_res!(pis, convert_vec_utf8),
    tag!("\"")
  )
);

named!(lex_string<&[u8], Token>,
    do_parse!(
        s: string >>
        (Token::StringLiteral(s))
    )
);



macro_rules! check(
  ($input:expr, $submac:ident!( $($args:tt)* )) => (
    {
      let mut failed = false;
      for &idx in $input {
        if !$submac!(idx, $($args)*) {
            failed = true;
            break;
        }
      }
      if failed {
        IResult::Error(ErrorKind::Custom(20))
      } else {
        IResult::Done(&b""[..], $input)
      }
    }
  );
  ($input:expr, $f:expr) => (
    check!($input, call!($f));
  );
);

// opcodes
fn parse_opcode(c: &str, rest: Option<&str>) -> Token {
    let mut string = c.to_owned();
    string.push_str(rest.unwrap_or(""));
    match string.as_ref() {
        "ADD" => Token::Opcode(String::from("ADD")),
        "SUB" => Token::Opcode(String::from("SUB")),
        "MUL" => Token::Opcode(String::from("MUL")),
        "DIV" => Token::Opcode(String::from("DIV")),
        "END" => Token::Opcode(String::from("END")),
        "PRT" => Token::Opcode(String::from("PRT")),
        "LOD" => Token::Opcode(String::from("LOD")),
        "JMP" => Token::Opcode(String::from("JMP")),
        "JMPR" => Token::Opcode(String::from("JMPR")),
        "JEQ" => Token::Opcode(String::from("JEQ")),
        "JNEQ" => Token::Opcode(String::from("JNEQ")),
        "CMP" => Token::Opcode(String::from("CMP")),
        "JZ" => Token::Opcode(String::from("JZ")),
        "JNZ" => Token::Opcode(String::from("JNZ")),
        "SLP" => Token::Opcode(String::from("SLP")),
        "IRO" => Token::Opcode(String::from("IRO")),
        // Loads a IRString
        "LODS" => Token::Opcode(String::from("LODS")),
        // Loads a Number
        "LODN" => Token::Opcode(String::from("LODN")),
        // Loads an Array
        "LODA" => Token::Opcode(String::from("LODA")),
        // Loads a Hash
        "LODH" => Token::Opcode(String::from("LODH")),
        _ => Token::Illegal,
    }
}

// integers
named!(lex_negative<&[u8], Token>,
    do_parse!(
        tag!("-") >>
        (Token::Negative)
    )
);

named!(lex_integer<&[u8], Token>,
    do_parse!(
        i: map_res!(map_res!(digit, str::from_utf8), FromStr::from_str) >>
        (Token::IntLiteral(i))
    )
);

named!(take_1_char, flat_map!(take!(1), check!(is_alphabetic)));

named!(lex_opcode<&[u8], Token>,
    do_parse!(
        c: map_res!(call!(take_1_char), str::from_utf8) >>
        rest: opt!(complete!(map_res!(alphanumeric, str::from_utf8))) >>
        (parse_opcode(c, rest))
    )
);

named!(lex_token<&[u8], Token>, alt_complete!(
    lex_labels |
    lex_register |
    lex_opcode |
    lex_punctuations |
    lex_string |
    lex_negative |
    lex_integer
));

named!(lex_tokens<&[u8], Vec<Token>>, ws!(many0!(lex_token)));

pub struct Lexer;

impl Lexer {
    pub fn lex_tokens(bytes: &[u8]) -> IResult<&[u8], Vec<Token>> {
        lex_tokens(bytes).map(|result| [&result[..], &vec![Token::EOF][..]].concat())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_lexer1() {
        let input = &b"ADD"[..];
        let result = Lexer::lex_tokens(input).to_result().unwrap();

        let expected_results = vec![
            Token::Opcode(String::from("ADD")),
            Token::EOF,
        ];

        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_register_start() {
        let input = &b"r100"[..];
        let result = register_start(input).to_result().unwrap();
        let expected_results = &b"r"[..];
        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_register() {
        let input = &b"r100"[..];
        let result = lex_register(input).to_result().unwrap();
        let expected_results = Token::Register(100);
        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_register_special() {
        let input = &b"eax"[..];
        let result = lex_register(input).to_result().unwrap();
        let expected_results = Token::SpecialRegister(String::from("eax"));
        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_instruction() {
        let input = &b"LOD 100 r0"[..];
        let result = Lexer::lex_tokens(input).to_result().unwrap();
        let expected_results = vec![
            Token::Opcode(String::from("LOD")),
            Token::IntLiteral(100),
            Token::Register(0),
            Token::EOF,
        ];
        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_label() {
        let input = &b"test: LOD 100 r0"[..];
        let result = Lexer::lex_tokens(input).to_result().unwrap();
        let expected_results = vec![
            Token::Label{ label: String::from("test") },
            Token::Opcode(String::from("LOD")),
            Token::IntLiteral(100),
            Token::Register(0),
            Token::EOF,
        ];
        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_negative() {
        let input = &b"LOD -100 r0"[..];
        let result = Lexer::lex_tokens(input).to_result().unwrap();
        let expected_results = vec![
            Token::Opcode(String::from("LOD")),
            Token::Negative,
            Token::IntLiteral(100),
            Token::Register(0),
            Token::EOF,
        ];
        assert_eq!(result, expected_results);
    }

    #[test]
    fn test_string() {
        let input = &b"LODS \"This is a test\""[..];
        let result = Lexer::lex_tokens(input).to_result().unwrap();
        let expected_results = vec![
            Token::Opcode(String::from("LODS")),
            Token::StringLiteral(String::from("This is a test")),
            Token::EOF,
        ];
        assert_eq!(result, expected_results);
    }
}
