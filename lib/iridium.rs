#[macro_use]
extern crate nom;
extern crate byteorder;
extern crate num_cpus;
extern crate time;

pub mod iasm;
pub mod iris;
pub mod vm;
pub mod repl;
